import sys
print('Current sys path:',sys.path)

import pytest
"""
Belowing shows how to add args to pytest
"""
def pytest_addoption(parser):
    parser.addoption("--name", action="store")

@pytest.fixture(scope='session')
def name(request):
    name_value = request.config.getoption("--name")
    if name_value is None:
        pytest.skip()
    return name_value

@pytest.fixture(scope='function')
def get_token(request):
    print "Get Token 0"
    return 0

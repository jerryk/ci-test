"""
Unit tests for the calculator library
"""

import src.jcmath.calculator as calculator


class TestCalculator:

    def test_addition(self):
       assert 4 == calculator.add(2, 2)

    def test_subtraction(self):
        assert 2 == calculator.subtract(4, 2)

def test_local_conftest(whoami):
    assert whoami == 'JerryKon'

def test_args(name):
    """This test would not run if no --name given
    Go to conftest.py for args detail.
    Args:
        name (TYPE): Description
    """
    assert name == 'almond'

def test_token1(get_token):
    assert get_token==0

def test_token2(get_token):
    assert get_token==0



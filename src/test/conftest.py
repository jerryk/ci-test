import pytest
from pytest_bdd import scenarios, given, when, then, parsers

scenarios('features/example.feature')

@pytest.fixture(scope='session')
def whoami():
    return "JerryKon"

@given('two nums {num1} and {num2} are given')
def give_num():
    print("Get nums %d %d"%(num1,num2))
 
# When Steps
 
@when(parsers.parse('the user searches for "{phrase}"'))
def search_phrase( phrase):
    print("Searching for phrase : %s"%phrase)
 
# Then Steps
 
@then(parsers.parse('results are shown for "{phrase}"'))
def search_results(whoami, phrase):
    print("Results are : %s %s"%(whoami,phrase))
